const connect = require('../db/connect');

module.exports = class dbController{

    static async getTablesNames(req, res){
        const queryShowTables = "show tables";

        connect.query(queryShowTables, async function(err, result, fields){
            if(err){
                console.log('Erro: ' + err);
                return res.status(500).json({error: "Erro ao obter tabelas do banco de dados"});
            }
            
            const tableNames = result.map(row => row[fields[0].name]);

            console.log("Tabelas do banco de dados: ", tableNames);
            res.status(200).json({message: "Tabelas do banco", tables: tableNames });    
        })
            
    }


    static async getTablesDescriptions(req, res){
        const queryShowTables = "show tables";

        connect.query(queryShowTables, async function(err, result, fields){
            if(err){
                console.log('Erro: ' + err);
                return res.status(500).json({error: "Erro ao obter tabelas do banco de dados"});
            }
            const tables = [];

            for(let i = 0; i < result.length; i++){

                const tableName = result[i]
                [`Tables_in_${connect.config.connectionConfig.database}`];

                
                const queryDescTables = `describe ${tableName}`;

                try {
                    const tableDescription = await new Promise((resolve, reject) => {
                        connect.query(queryDescTables, function (err, result, fields){
                            if(err){
                                reject(err);
                            }
                            resolve(result);
                        })
                    });

                    tables.push({name: tableName, description: tableDescription})
                } catch (error) {
                    console.log(error);
                    return res.status(500).json({error: "Error ao obter a descrição da tabela!"});
                }

            }

            res.status(200).json({message: "Obtendo todas as tabelas de suas descrições ", tables});
        })

    }
}
