const router = require("express").Router();
const dbController = require("../controller/dbController");

router.get("/tables/names", dbController.getTablesNames);
router.get("/tables/descriptions", dbController.getTablesDescriptions);

module.exports = router;